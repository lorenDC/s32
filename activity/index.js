let http = require("http");

let courses = [
	{
		"course_name": "BS Math",
		"years": "4 years"
	},
	{
		"course_name": "BS IT",
		"years": "4 years"
	}
];


let port = 4000;

let server = http.createServer(function(req, res){
	if(req.url == '/profile' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end("Welcome to your Profile!")
	} else if (req.url == '/courses' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.write(JSON.stringify(courses))
		res.end("Here's our courses available")
	} else if (req.url == '/addcourse' && req.method == 'POST') {
		let request_body = ' '

		req.on('data', function(data){
			request_body += data
			console.log(request_body)
		})
		req.on('end', function() {
			console.log(typeof request_body)

			request_body = JSON.parse(request_body)

			let new_course = {
				"course_name": request_body.course_name,
				"years": request_body.years
			}
			courses.push(new_course)

			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(new_course))
			res.end("Added a course to our resources")
		})
	} else if (req.url == '/updatecourse' && req.method == 'PATCH') {
		let request_body = ' '

		req.on('data', function(data){
			request_body += data
			console.log(request_body)
		})
		req.on('end', function() {
			console.log(typeof request_body)

			request_body = JSON.parse(request_body)

			let new_course = {
				"course_name": request_body.course_name,
				"years": request_body.years
			}
			courses.push(new_course)

			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(new_course))
			res.end("Update a course to our resources")
		})
	} else if (req.url == '/archivecourse' && req.method == 'DELETE') {
		let request_body = ' '

		req.on('data', function(data){
			request_body += data
			console.log(request_body)
		})
		req.on('end', function() {
			console.log(typeof request_body)

			request_body = JSON.parse(request_body)

			let new_course = {
				"name": request_body.name,
				"email": request_body.email
			}
			courses.push(new_course)

			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(new_course))
			res.end()
		})
	} else {
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end("Welcome to Booking System!")
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);