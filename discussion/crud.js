let http = require("http");

let users = [
	{
		"name": "Jose Mari Chan",
		"email": "jmarichan@gmail.com"
	},
	{
		"name": "Mariah Carey",
		"email": "mariahwhistle@gmail.com"
	}
];


let port = 4000;

let server = http.createServer(function(req, res){
	if(req.url == '/users' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.write(JSON.stringify(users))
		res.end()
	} else if (req.url == '/users' && req.method == 'POST') {
		let request_body = ' '

		req.on('data', function(data){
			request_body += data
			console.log(request_body)
		})
		req.on('end', function() {
			console.log(typeof request_body)

			request_body = JSON.parse(request_body)

			let new_user = {
				"name": request_body.name,
				"email": request_body.email
			}
			console.log(new_user)
			users.push(new_user)
			console.log(users)

			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(new_user))
			res.end()
		})
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);